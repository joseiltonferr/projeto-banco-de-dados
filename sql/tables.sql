create table empreendimento (
	cod_empreendimento serial primary key,
	nome varchar(30) not NULL,
	tipo varchar(2) not NULL,
	cpf varchar(14) unique NULL,
	cnpj varchar(18) unique NULL
);

create table processo (
	no_processo varchar(12) unique not NULL,
	cod_empreendimento int not NULL,
	data_protocolo date default current_date,
	fase text not NULL,
	primary key (no_processo, cod_empreendimento),
	foreign key (cod_empreendimento) references empreendimento(cod_empreendimento)
);

create table auto_infracao (
	no_auto serial,
	no_processo varchar(12) NULL,
	cod_empreendimento int not NULL,
	descricao text not NULL,
	valor_multa numeric not NULL,
	data_auto date default current_date,
	primary key (no_auto, no_processo, cod_empreendimento),
	foreign key (no_processo, cod_empreendimento) references processo (no_processo, cod_empreendimento)
);

create table localizacao (
	estado varchar(2) not NULL,
	cidade varchar(30) not NULL,
	primary key (cidade, estado)
);

create table substancia (
	nome varchar(30) not NULL,
	uso varchar(30) not NULL,
	primary key (nome, uso)
);

create table localizacao_processo (
	estado_localizacao varchar(2) not NULL,
	cidade_localizacao varchar(30) not NULL,
	no_processo varchar(12) not NULL,
	cod_empreendimento int not NULL,
	primary key (estado_localizacao, cidade_localizacao, no_processo, cod_empreendimento),
	foreign key (no_processo, cod_empreendimento) references processo (no_processo, cod_empreendimento),
	foreign key (estado_localizacao, cidade_localizacao) references localizacao (estado, cidade)
);

create table substancia_processo (
	uso_substancia varchar(30) not NULL,
	nome_substancia varchar(30) not NULL,
	no_processo varchar(12) not NULL,
	cod_empreendimento int not NULL,
	primary key (uso_substancia, nome_substancia, no_processo, cod_empreendimento),
	foreign key (no_processo, cod_empreendimento) references processo (no_processo, cod_empreendimento),
	foreign key (uso_substancia, nome_substancia) references substancia (uso, nome)
);

create table evento (
	cod_evento serial primary key,
	descricao text not NULL,
	data_evento date default current_date,
	no_processo varchar(12) not NULL,
	cod_empreendimento int not NULL,
	foreign key (no_processo, cod_empreendimento) references processo (no_processo, cod_empreendimento)
);

-- INSERÇÃO DE DADOS INICIAIS PARA TESTES --

-- SUBSTÂNCIAS --
insert into substancia (nome, uso)
values
('areia', 'construção civil'),
('areia', 'industrial'),
('cascalho', 'construção civil'),
('argila', 'construção civil'),
('argila', 'cerâmica'),
('argila', 'industrial'),
('diamante', 'gema'),
('diamante', 'industrial'),
('quartzo', 'industrial'),
('feldspato', 'industrial'),
('calcário', 'brita'),
('calcário', 'revestimento'),
('calcário', 'corretivo de solo'),
('granito', 'brita'),
('granito', 'revestimento'),
('água mineral', 'engarrafamento');

-- LOCALIZACÕES --
insert into localizacao (estado, cidade)
values
('pb', 'joão pessoa'),
('pb', 'santa rita'),
('pb', 'bayeux'),
('pb', 'cabedelo'),
('pb', 'campina grande'),
('pb', 'lagoa seca'),
('pb', 'esperança'),
('mg', 'coromandel'),
('mg', 'uberlândia'),
('pe', 'recife');

-- EMPREENDIMENTOS --
insert into empreendimento (nome, tipo, cpf, cnpj)
values
('Mineração Paraíba', 'pj', NULL, '00.001.002/0001-04'),
('Mineração Pernambuco', 'pj', NULL, '10.100.100/0001-10'),
('Joseilton Ferreira', 'pf', '000.000.000-00', NULL),
('João Silva', 'pf', '111.111.111-11', NULL),
('João Silva', 'pf', '222.222.222-22', NULL),
('Mineração Areia', 'pj', NULL, '99.999.999/0001-99');
-- TUPLA DE TEXTO
create type tp_text as (var_01 text, var_02 text);

-- FUNCAO ADD EVENTO --
/*create or replace function add_evento(_processo int, _desc text)
returns void as $$
declare
	v_p record;
	v_d record;
begin
	-- v_p
	select * into v_p from processo where processo.no_processo = _processo;
	-- v_d
	select current_date into v_d;
	-- evento
	insert into evento (descricao, data_evento, no_processo, cod_empreendimento)
	values (_desc, v_d.current_date, v_p.no_processo, v_p.cod_empreendimento);
end;
$$ language plpgsql;*/

----------------------------------------------------------------------------------------------------------------------------

-- FUNCAO ADD PROCESSO --
create or replace function add_processo(_pro text, _emp text, _sub tp_text[], _loc tp_text[], _fase text)
returns void as $$
declare
	v_e record;
	_cid text;
	_est text;
	_nsb text;
	_uso text;
begin
	-- v_e
	select * into v_e from empreendimento where cnpj = _emp or cpf = _emp;
	-- processo
	insert into processo (no_processo, cod_empreendimento, fase)
	values (_pro, v_e.cod_empreendimento, _fase);
	-- v_p
	-- substancia_processo
	foreach _nsb, _uso in array _sub loop
		insert into substancia_processo (nome_substancia, uso_substancia, no_processo, cod_empreendimento)
		values (_nsb, _uso, _pro, v_e.cod_empreendimento);
	end loop;
	-- localizacao_processo
	foreach _cid, _est in array _loc loop
		insert into localizacao_processo (cidade_localizacao, estado_localizacao, no_processo, cod_empreendimento)
		values (_cid, _est, _pro, v_e.cod_empreendimento);
	end loop;
	-- evento protocolo
	insert into evento (descricao, no_processo, cod_empreendimento)
	values ('requerimento protocolado', _pro, v_e.cod_empreendimento);
end;
$$ language plpgsql;
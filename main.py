import postgresql
import os

# limpa o prompt
def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')
    return

# inseri empreendimento
def insert_emp():
    clear()
    print(f"{'*'*140}")
    print('Inserir Empreendimento')
    nm = input('Nome: ')
    emp = input('CPF/CNPJ: ')
    tp = input('Tipo(pj/pf): ')
    print(f"{'*'*140}")
    if tp == 'pf':
        cmd = ' '.join(("insert into empreendimento (nome, tipo, cpf)",
                        f"values ('{nm}', '{tp}', '{emp}')"))
    elif tp == 'pj':
        cmd = ' '.join(("insert into empreendimento (nome, tipo, cnpj)",
                        f"values ('{nm}', '{tp}', '{emp}')"))
    db.execute(cmd)
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# inseri auto de infracao
def insert_auto():
    clear()
    print(f"{'*'*140}")
    print('Inserir Auto de Infração')
    npc = input('N° Processo: ')
    dsc = input('Descrição: ')
    mlt = float(input('Valor da Multa: '))
    print(f"{'*'*140}")
    cmd = ' '.join(("select cod_empreendimento from processo",
                    f"where no_processo ='{npc}'"))
    cod_e = db.prepare(cmd).first()
    cmd = ' '.join(("insert into auto_infracao (no_processo, cod_empreendimento, descricao, valor_multa)",
                    f"values ('{npc}', {cod_e}, '{dsc}', {mlt})"))
    db.execute(cmd)
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# consulta e inseri evento
def insert_get_evento(mode):
    clear()
    print(f"{'*'*140}")
    npc = input('N° Processo: ')
    if mode == 'insert':
        dsc = input('Descrição: ')
        print(f"{'*'*140}")
        cmd = ' '.join(("select cod_empreendimento from processo",
                        f"where no_processo ='{npc}'"))
        cod_e = db.prepare(cmd).first()
        cmd = ' '.join(("insert into evento (descricao, no_processo, cod_empreendimento)",
                        f"values ('{dsc}', '{npc}', {cod_e})"))
        db.execute(cmd)
    elif mode == 'get':
        cmd = ' '.join(("select e.no_processo, e.data_evento, e.descricao from evento as e",
                        f"where e.no_processo = '{npc}'"))
        out = db.prepare(cmd)
        hd = ['N° Processo', 'Data', 'Descrição']
        print(f"{'='*140}")
        print(f'{hd[0]:14} {hd[1]:12} {hd[2]}')
        print(f'{"-"*12}   {"-"*10}   {"-"*15}')
        for i in out:
            print(f'{i[0]:14} {i[1]}{" "*3}{i[2]}')
        print(f"{'='*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# inseri processo mineral no banco de dados
def insert_pro():
    clear();
    print(f"{'*'*140}")
    npc = input('N° Processo: ')
    emp = input('CPF/CNPJ Empeendimento: ')
    fase = input('Fase: ')
    cmd = ' '.join((f"select cod_empreendimento from empreendimento",
                    f"where cnpj ='{emp}' or cpf = '{emp}'"))
    cod_e = db.prepare(cmd).first()
    # processo - empreendimento
    cmd = ' '.join(("insert into processo (no_processo, cod_empreendimento, fase)",
                    f"values ('{npc}', {cod_e}, '{fase}')"))
    pro = db.execute(cmd)
    # processo - substancia
    while True:
        n_sub = input('Substancia: ')
        u_sub = input('Uso: ')
        cmd = ' '.join(("insert into substancia_processo",
                        f"values ('{u_sub}', '{n_sub}', '{npc}', {cod_e})"))
        p_s = db.execute(cmd)
        key = input('Inserir outra substância (y or N):')
        if key.lower() != 'y':
            break
    # processo - localizacao
    while True:
        c_loc = input('Cidade: ')
        e_loc = input('Estado(Sigla): ')
        cmd = ' '.join(("insert into localizacao_processo",
                        f"values ('{e_loc}', '{c_loc}', '{npc}', {cod_e})"))
        p_l = db.execute(cmd)
        key = input('Inserir outra localização (y or N):')
        if key.lower() != 'y':
            break
    # processo - evento
    cmd = ' '.join(("insert into evento (descricao, no_processo, cod_empreendimento)",
                    f"values ('requerimento protocolado', '{npc}', {cod_e})"))
    db.execute(cmd)
    print(f"{'*'*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# consulta empreendimento
def get_emp():
    clear()
    print(f"{'*'*140}")
    emp = input('CPF/CNPJ: ') or None
    if emp != None:
        cmd = ' '.join(("select cod_empreendimento, nome, coalesce(cpf, cnpj) as CPF_CNPJ, tipo",
                        "from empreendimento",
                        f"where cnpj = '{emp}' or cpf = '{emp}'"))
        out = db.prepare(cmd)
    else:
        cmd = "select cod_empreendimento, nome, coalesce(cpf, cnpj) as CPF_CNPJ, tipo from empreendimento"
        out = db.prepare(cmd)
    hd= ['Código', 'Nome', 'CPF/CNPJ', 'Tipo']
    print(f"{'*'*140}\n{'='*140}")
    print(f'{hd[0]:8} {hd[1]:32} {hd[2]:20} {hd[3]:4}')
    print('-'*6, ' ', '-'*30, ' ', '-'*18, ' ', '-'*4)
    for i in out:
        print(f'{i[0]:<8} {i[1]:32} {i[2]:20} {i[3]:}')
    print(f"{'='*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# consulta auto de infracao
def get_auto(mode):
    clear()
    print(f"{'*'*140}")
    if mode == 'emp':
        emp = input('CPF/CNPJ: ')
        cmd_cp = f"where e.cnpj ='{emp}' or e.cpf = '{emp}'"
    elif mode == 'proc':
        npc = input('N° Processo: ')
        cmd_cp = f"where a.no_processo = '{npc}'"
    elif mode == 'date':
        print('Formato da Data (YYYY-MM-DD)')
        idt = input('Data Inicial: ') or None
        fdt = input('Data Final: ') or None
        if fdt == None and idt == None:
            cmd_cp = ""
        elif idt == None:
            cmd_cp = f"where a.data_auto <= '{fdt}'"
        elif fdt == None:
            cmd_cp = f"where a.data_auto >= '{idt}'"
        else:
            cmd_cp = f"where a.data_auto between '{idt}' and '{fdt}'"

    cmd = ' '.join(("select a.no_auto, a.no_processo, e.nome, coalesce(e.cpf, e.cnpj) as CPF_CNPJ,",
                    "a.data_auto, a.valor_multa, a.descricao from auto_infracao as a",
                    "join empreendimento as e on e.cod_empreendimento = a.cod_empreendimento", cmd_cp,
                    "order by a.no_auto"))
    out = db.prepare(cmd)
    hd = ['N° Auto', 'N° Processo', 'Empreendimento', 'CPF/CNPJ', 'Data', 'Multa (R$)', 'Descrição']
    print(f"{'*'*140}\n{'='*140}")
    print(f"{hd[0]:9} {hd[1]:14} {hd[2]:32} {hd[3]:20} {hd[4]:12} {hd[5]:12} {hd[6]}")
    print(f"{'-'*7}   {'-'*12}   {'-'*30}   {'-'*18}   {'-'*10}   {'-'*10}   {'-'*15}")
    for i in out:
        print(f"{i[0]:<9} {i[1]:14} {i[2]:32} {i[3]:20} {i[4]} {i[5]:12}   {i[6]}")
    print(f"{'='*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# consulta relatorio auto de infracao
def get_relat():
    clear()
    print(f"{'*'*140}")
    print('Inserir data no formato(YYYY-MM-DD)')
    idt = input('Data Inicial: ') or None
    fdt = input('Data Final: ') or None
    if fdt == None and idt == None:
        cmd_cp = ""
    elif idt == None:
        cmd_cp = f"where a.data_auto <= '{fdt}'"
    elif fdt == None:
        cmd_cp = f"where a.data_auto >= '{idt}'"
    else:
        cmd_cp = f"where a.data_auto between '{idt}' and '{fdt}'"
    cmd = ' '.join(("select descricao, count(a.no_processo) as proc, count(a.cod_empreendimento) as emp",
                    "from auto_infracao as a", cmd_cp, "group by descricao order by proc desc"))
    out = db.prepare(cmd)
    hd = ['Tipo de Infração', 'Qtd de Processos', 'Qtd de Empreendimentos']
    print(f"{'*'*140}\n{'='*140}")
    print(f"{hd[0]:42} {hd[1]:20} {hd[2]}")
    print(f"{'-'*40}   {'-'*16}     {'-'*22}")
    for i in out:
        print(f'{i[0]:42} {i[1]:<20} {i[2]:<22}')
    print(f"{'='*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# consulta relatorio geral
def get_sit():
    clear()
    print(f"{'*'*140}")
    emp = input('CPF/CNPJ: ') or None
    if emp == None:
        cmd_cp = ""
    else:
        cmd_cp = f"where e.cpf = '{emp}' or e.cnpj = '{emp}'"
    cmd = ' '.join(("select coalesce(e.cpf, e.cnpj) as cpf_cnpj, e.nome, count(p.no_processo) as qt_processo,",
                    "count(a.no_processo) as qt_pc_auto, count(a.no_auto) as qt_auto,",
                    "(count(a.no_auto)::numeric/count(p.no_processo))::numeric(5,2) as prop",
                    "from processo as p",
                    "join empreendimento as e on p.cod_empreendimento = e.cod_empreendimento",
                    "left join auto_infracao as a on a.no_processo = p.no_processo", cmd_cp,
                    "group by e.nome, cpf_cnpj",
                    "order by qt_processo desc"))
    out = db.prepare(cmd)
    hd = ['CPF/CNPJ', 'Empreendimento', 'Qtd de Processos',  'Qtd Proc. Autoados', 'Qtd de Autos',
          'Qtd de Autos / Qtd Processos']
    print(f"{'*'*140}\n{'='*140}")
    print(f"{hd[0]:20} {hd[1]:32} {hd[2]:18} {hd[3]:20} {hd[4]:14} {hd[5]}")
    print(f"{'-'*18}   {'-'*30}   {'-'*16}   {'-'*18}   {'-'*12}   {'-'*28}")
    for i in out:
        print(f'{i[0]:20} {i[1]:32} {i[2]:<18} {i[3]:<20} {i[4]:<14} {i[5]}')
    print(f"{'='*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# consulta processo
def get_pro(mode=None):
    clear()
    print(f"{'*'*140}")
    if mode == None:
        npc = input('Processo: ') or None
        if npc != None:
            cmd_cp = f"where p.no_processo = '{npc}'"
        else:
            cmd_cp = ""
    elif mode == 'emp':
        emp = input('CPF/CNPJ: ')
        if emp == None:
            cmd_cp = ""
        else:
            cmd_cp = f"where e.cnpj = '{emp}' or e.cpf = '{emp}'"
    elif mode == 'loc':
        c_loc = input('Cidade: ') or None
        e_loc = input('Estado(Sigla): ') or None
        if c_loc == None and e_loc == None:
            cmd_cp = ""
        elif c_loc == None:
            cmd_cp = f"where l.estado_localizacao = '{e_loc}'"
        elif e_loc == None:
            cmd_cp = f"where l.cidade_localizacao = '{c_loc}'"
        else:
            cmd_cp = f"where l.estado_localizacao = '{e_loc}' and l.cidade_localizacao = '{c_loc}'"
    elif mode == 'sub':
        n_sub = input('Substância: ') or None
        u_sub = input('Uso: ') or None
        if n_sub == None and u_sub == None:
            cmd_cp = ""
        elif n_sub == None:
            cmd_cp = f"where s.uso_substancia = '{u_sub}'"
        elif u_sub == None:
            cmd_cp = f"where s.nome_substancia = '{n_sub}'"
        else:
            cmd_cp = f"where s.uso_substancia = '{u_sub}' and s.nome_substancia = '{n_sub}'"
    cmd = ' '.join(("select p.no_processo, e.nome, coalesce(e.cpf, e.cnpj) as CPF_CNPJ, s.nome_substancia,",
                    "s.uso_substancia, l.cidade_localizacao, l.estado_localizacao, p.data_protocolo, p.fase",
                    "from processo as p",
                    "join localizacao_processo as l on l.no_processo = p.no_processo",
                    "join substancia_processo as s on s.no_processo = p.no_processo",
                    "join empreendimento as e on p.cod_empreendimento = e.cod_empreendimento", cmd_cp))
    out = db.prepare(cmd)
    hd= ['N° Processo', 'Empreendimento', 'CPF/CNPJ', 'Substância', 'Uso', 'Localizacao', 'Data', 'Fase']
    print(f"{'*'*140}\n{'='*140}")
    print(f'{hd[0]:13} {hd[1]:26} {hd[2]:19} {hd[3]:12} {hd[4]:17} {hd[5]:20} {hd[6]:12} {hd[7]}')
    print(f"{'-'*12}  {'-'*25}  {'-'*18}  {'-'*11}  {'-'*16}  {'-'*19}  {'-'*10}  {'-'*15}")
    for i in out:
        print(f"{i[0]:13} {i[1]:26} {i[2]:19} {i[3]:12} {i[4]:17} {i[5]+'/'+i[6]:20} {i[7]}  {i[8]}")
    print(f"{'='*140}")
    ex = input('Pressioner qualquer tecla para voltar...')
    return

# conecta com banco de dados
db = postgresql.open('pq://postgres:1234@localhost:5432/projeto_db')

# menu
while True:
    clear()
    print(f"{'*'*140}\n{' '*63}Banco de Dados{' '*63}\n{'*'*140}\n")
    print(f"[ 1]\tInserir Novo Empreendimento")
    print(f"[ 2]\tInserir Novo Processo Mineral")
    print(f"[ 3]\tInserir Evento a Processo")
    print(f"[ 4]\tInserir Auto de Infração")
    print(f"[ 5]\tConsultar Empreendimento")
    print(f"[ 6]\tConsultar Processo")
    print(f"[ 7]\tConsultar Processo por Empreendimento")
    print(f"[ 8]\tConsultar Processo por Substância Mineral")
    print(f"[ 9]\tConsultar Processo por Localização")
    print(f"[10]\tConsultar Eventos de Processo")
    print(f"[11]\tConsultar Autos de Infração por Processo")
    print(f"[12]\tConsultar Autos de Infração por Empreendimento")
    print(f"[13]\tConsultar Autos de Infração por Período")
    print(f"[14]\tConsultar Relatório Autos de Infração")
    print(f"[15]\tConsultar Relatório Geral Empreendimento")
    print(f"[16]\tSair")
    print(f"\n{'*'*140}\n")
    opt = input(f">> Opção Escolhida: ")
    if opt == '1':
        insert_emp()
    elif opt == '2':
        insert_pro()
    elif opt == '3':
        insert_get_evento('insert')
    elif opt == '4':
        insert_auto()
    elif opt == '5':
        get_emp()
    elif opt == '6':
        get_pro()
    elif opt == '7':
        get_pro('emp')
    elif opt == '8':
        get_pro('sub')
    elif opt == '9':
        get_pro('loc')
    elif opt == '10':
        insert_get_evento('get')
    elif opt == '11':
        get_auto('proc')
    elif opt == '12':
        get_auto('emp')
    elif opt == '13':
        get_auto('date')
    elif opt == '14':
        get_relat()
    elif opt == '15':
        get_sit()
    elif opt == '16':
        break

# encerra a conexao
db.close()

